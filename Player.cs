namespace MainClasses4QGame
{
    public class Player
    {
        public int health = 50;
        public int damage = 20;
        public int stealth = 3;
        public string charName;
        public string spec;
        public bool canPassThroughEnemy;

        public Player(string name,string spec)
        {
            charName = name;
            this.spec = spec;
            if(spec == "Маг")
            {
                damage += 10;
                health -= 10;
                stealth -= 1;
                canPassThroughEnemy = true;

            }
            if(spec == "Воин")
            {
                canPassThroughEnemy = false;
            }
            if(spec == "Вор")
            {
                damage -= 15;
                health -= 15;
                stealth += 3;
                canPassThroughEnemy = true;
            }
        }

        /// <summary>
        /// Функция, которая рассчитывает возможность побега для персонажа игрока, основываясь на скрытности персонажа и ловкости врага.
        /// </summary>
        /// <param name="enemyAgility">Параметр ловкости врага, будет передаваться из объекта класс Enemy.</param>
        /// <returns>Возвращает True, если сбежать удалось, и False, если не удалось.</returns>
        public bool getAway(int enemyAgility)
        {
            if(enemyAgility > this.stealth)
                return false;
            return true;
        }

        public override string ToString()
        {
            return "Имя персонажа: " + this.charName + "\n"
                +  "Класс: " + this.spec + "\n"
                +  "Урон: "  + this.damage + "\n"
                +  "Здоровье: " + this.health + "\n"
                +  "Скрытность: " + this.stealth + "\n";
        }

    }
}
