using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainClasses4QGame
{
    public class Enemy
    {
        public string enemyName;
        public int health;
        public int damage;
        public int agility;

        List<string> eNames = new List<string>() {"Огнедышащий Давуд", "Пивной Вадик (который лучик)", "Шурлин Тёма (Очень смертельный)" };

        public Enemy()
        {
            Random rnd = new Random();
            int num = rnd.Next(0,3);
            if (num == 0)
            {
                enemyName = eNames[num];
                health = 20;
                agility = 7;
                damage = 10;
            }
            if(num == 1)
            {
                enemyName = eNames[num];
                health = 70;
                agility = 3;
                damage = 30;
            }
            if(num == 2)
            {
                enemyName = eNames[num];
                health = 999;
                agility = 10;
                damage = 999;
            }
        }

        public override string ToString()
        {
            return "Имя врага: " + this.enemyName + "\n"
                + "Урон: " + this.damage + "\n"
                + "Здоровье: " + this.health + "\n"
                + "Ловкость: " + this.agility + "\n";
        }
    }
}
