using MainClasses4QGame;
using System.Linq;

namespace Семинар_на_практику
{
    public partial class Form1 : Form
    {
        bool flag;
        bool flag1;
        public Player player;
        Enemy enemy = new Enemy();
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                textBox1.Text = "Введите что-нибудь :3";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            richTextBox1.Visible = true;
            textBox1.Visible = true;
            button1.Visible = true;
            button2.Visible = true;
            button3.Visible = true;
            button4.Visible = false;
            button5.Visible = true;
            richTextBox1.Text = "Добрый день, игрок. Сейчас тебе будет предложено выбрать 1 из 3 классов для твоего персонажа. Воины — класс персонажей, сражающихся в ближнем бою и способных исполнять роль как танков, так и бойцов." +
               " Маг - класс, использующий оружие, наносящее магический урон, а также специальную броню, которая повышает силу магических атак, запас маны, шанс критического удара магическими атаками. Воры отличаются от других классов пониженной защитой и высоким уроном. " +
               "Они обычно являются скрытными и умными персонажами и быстрыми, способными обезвреживать ловушки, взламывать замки и замки, чтобы совершать карманные кражи , шпионить за врагами, избегать обнаружения со стороны врага и наносить удары в спину , а также прятаться в тени." + "\n" + "" + "\n" +
               "Итак, пора создать своего персонажа. Нажми на кнопку <<создать персонажа>>";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            button6.Visible = true;
            button5.Visible = false;
        }
        
        private void button6_Click(object sender, EventArgs e)
        {
            
            if (textBox1.Text == "")
            {
                MessageBox.Show("Назовите своего персонажа");
            }
            if (comboBox1.SelectedItem == null)
            {
                MessageBox.Show("Выберите класс своего персонажа");
            }
             
            Player player1 = new Player(textBox1.Text, comboBox1.Text);

            panel1.Visible = false;
            button6.Visible = false;

            richTextBox1.AppendText ("\n" + "Отлично, вы смогли создать своего персонажа c именем " + player1.charName + " и классом " + player1.spec + "\n" + "Пора начинать" +"\n");
            richTextBox1.AppendText("Вы очнулись в грязном и очень неприятном помещении. Перед вами лестница, которая выглядит очень шатко и не вызывает какого-либо доверия, а так же тяжёлая дубовая дверь. Остаётся только выбрать куда вам идти.");
            button7.Visible = true;
            button8.Visible = true;

        }
  
        public void button7_Click(object sender, EventArgs e)
        {
            Player player1 = new Player(textBox1.Text, comboBox1.Text);
            flag = true;
            if (flag == true)
            {
                button7.Visible = false;
                button8.Visible = false;

                richTextBox1.AppendText("\n" + "Вы подошли к лестнице и взглянули наверх. Вверху вы видите белый свет. Свобода, подумали вы. Начав подниматься по лестнице вы замечаете, что лестница начинает скрипеть и шататься. Добравшись почти на половину до заветной свободы, лестница ломается и вы падаете вниз. Приземление точно не покпзплось вам мягким. (-10hp)");
                player1.health -= 10;
                flag = false;
                flag1 = true;
                if (flag1 == true)
                {
                    button7.Visible = false;
                    button8.Visible = false;
                    richTextBox1.AppendText("\n" + "\n" + "Вы прошли в дверь. Впереди вас ждал только мрак и длинный коридор. Пройдя метров 10, вы замечаете еле горящий костёр. За ним сидел " + enemy.enemyName + ".\n" + "Что же делать?");
                    button9.Visible = true;
                    button10.Visible = true;
                    button11.Visible = true;


                }

            }

        }

        private void button8_Click(object sender, EventArgs e)
        {
            flag1 = true;
            if (flag1 == true)
            {
                button7.Visible = false;
                button8.Visible = false;
                richTextBox1.AppendText("\n" + "Вы прошли в дверь. Впереди вас ждал только мрак и длинный коридор. Пройдя метров 10, вы замечаете еле горящий костёр. За ним сидел " + enemy.enemyName + ".\n" + "Что же делать?");
                button9.Visible = true;
                button10.Visible = true;
                button11.Visible = true;
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Player player1 = new Player(textBox1.Text, comboBox1.Text);
            if(player1.spec == "Вор")
            {
                richTextBox1.AppendText("\n" + "Ты оказался дорстаточно ловким, чтобы " + enemy.enemyName + " тебя не заметил." + "\n" + "Оказалось, что враг охранят выход из подземелья. Свобода... Долгожданная свобода.");
                MessageBox.Show("Ты смог вырваться из лап смерти, поздравляем!");
                this.Close();

            }
            else
            {
                richTextBox1.AppendText("\n" + "Попытка пройти незаметно провалилась. Ты и сам не заметил как враг нанёс по тебе смертельный удар. А ведь свобода была так близка...");
                MessageBox.Show("Ты окончил свой путь, не успев его начать.");
                this.Close();
            }

        }

        private void button9_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText("\n" + "Вы решили что лучше всего будет убежать.");
            Player player1 = new Player(textBox1.Text, comboBox1.Text);
            if(player1.canPassThroughEnemy == false)
            {
                richTextBox1.AppendText("\n" + "Ваши доспехи мешают вам уйти. Вспомнив слова Наруто о том, что никогда нельзя сдаваться, вы направляетесь в сторону врага." + "\n" + "****************" + "\n" + "Сейчас должен бы быть бой, но программистам мало платят." + "\n" + "\n"+ "****************");
                MessageBox.Show("Конец(");
                this.Close();
            }
            if(player1.canPassThroughEnemy == true)
            {
                richTextBox1.AppendText("\n" + "Вы смогли убежать от страшного монстра в виде " + enemy.enemyName + " . Пробежав достаточно далеко по неизведанному пути, вы замечатете спуск в подвал и какой-то странный свет впереди коридора. Враг уже поджимает сзади, что же делать?");
                button9.Visible = false;
                button10.Visible = false;
                button11.Visible = false;
                button12.Visible = true;
                button13.Visible = true;

            }

        }

        private void button10_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText("\n" + "Вспомнив слова Наруто о том, что никогда нельзя сдаваться, вы направляетесь в сторону врага." + "\n" + "****************" + "\n" + "Сейчас должен бы быть бой, но программистам мало платят." + "\n" + "\n" + "****************");
            MessageBox.Show("Этот бой был просто невообразим, жаль, что программистам мало платят ((((");
            this.Close();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Player player1 = new Player(textBox1.Text, comboBox1.Text);

            richTextBox1.AppendText("\n" + "Вы решаете пойти в подвал, забежав внутрь вы закрываете за собой дверь на замок. Думая что всё позади, вы зажигаете спичку и замираете от ужаса. Впереди вас стоят 5 " + player1.charName + "Это конец..");
            MessageBox.Show("Кто вообще додумывается идти в подвалы? Вы что, не смотрели фильмы ужасов?");
            this.Close();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText("\n" + "Вы направляетесь прямиком к свету. Думая, что это конец и вы никогда не выберетесь из этого кошмара, вы натыкаетесь на комнату, в которой есть единственная дверь. Открыв её вы оказываетесь на улице. Удача? Нет, задумка сценаристов.");
            MessageBox.Show("Повезло вашему персонажу, что мы такие добрые :3");
            this.Close();
        }
    }
}